import os

from psycopg2.extras import DictCursor


class ConfigError(KeyError):
    pass


try:
    DB_NAME = os.environ.get('DB_NAME')
    DB_USER = os.environ.get('DB_USER')
    DB_PASSWORD = os.environ.get('DB_PASSWORD')
    DB_PORT = int(os.environ.get('DB_PORT'))
    DB_HOST = os.environ.get('DB_HOST', '127.0.0.1')

    DB_SQLITE_PATH = os.environ.get('DB_PATH')

    POSTGRES_CONNECTION_CONFIG = {
        'dbname': DB_NAME,
        'user': DB_USER,
        'password': DB_PASSWORD,
        'host': DB_HOST,
        'port': DB_PORT,
        'cursor_factory': DictCursor,
    }

except KeyError as e:
    raise ConfigError(f"Unable to found environment variable {str(e)}") from e
