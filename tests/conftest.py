import contextlib

import psycopg2
import pytest

from core import config
from utils.utils import sqlite_connection


@pytest.fixture
def connections():
    with sqlite_connection(config.DB_SQLITE_PATH) as sqlite_conn, contextlib.closing(
            psycopg2.connect(**config.POSTGRES_CONNECTION_CONFIG)
    ) as pg_conn:
        yield pg_conn, sqlite_conn
