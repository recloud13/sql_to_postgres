import dataclasses

from pytest import mark

from databases.postgre import PostgresMigration
from databases.sqlite import SQLiteMigration
from models.base import LoadData
from models.domain import Person, Genre, FilmWork, GenreFilmWork, PersonFilmWork

load_queries = [
    LoadData('person', Person, 'SELECT * from person'),
    LoadData('genre', Genre, 'SELECT * from genre'),
    LoadData('film_work', FilmWork, 'SELECT * from film_work'),
    LoadData('genre_film_work', GenreFilmWork, 'SELECT * FROM genre_film_work'),
    LoadData('person_film_work', PersonFilmWork, 'SELECT * FROM person_film_work'),
]


@mark.parametrize('load_data', load_queries)
def test_some(load_data: LoadData, connections):
    postgres_connection, sqlite_connection = connections
    postgres = PostgresMigration(postgres_connection)
    sqlite = SQLiteMigration(sqlite_connection)

    postgres_data = postgres.load_all_data(load_data)
    sqlite_data = sqlite.load_all_data(load_data)

    assert len(postgres_data) == len(sqlite_data)

    for postgres_item, sqlite_item in zip(postgres_data, sqlite_data):
        assert dataclasses.asdict(postgres_item) == dataclasses.asdict(sqlite_item)



