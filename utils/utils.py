import datetime
import sqlite3
from contextlib import contextmanager
from typing import Union, Optional, Type, List

from dateutil.parser import parser


@contextmanager
def sqlite_connection(db_path: str):
    """Контекстный менеджер для создания connection SQLite3"""
    conn = sqlite3.connect(db_path)
    conn.row_factory = sqlite3.Row

    yield conn

    conn.close()


def join_list_to_string(target: List[str]):
    """ Формирование строки из списка через запятую"""
    return ', '.join(target)


def parse_datetime(target: Optional[Union[str, datetime.datetime]]):
    """Подготовка временных данных"""
    if not target:
        target = datetime.datetime.utcnow()

    if target and isinstance(target, str):
        target = convert_str_to_datetime(target)

    target = target.replace(tzinfo=None)

    return target


def convert_str_to_datetime(datetime_str: str) -> datetime.datetime:
    """Преобразование даты в строковом виде в datetime"""
    return parser().parse(datetime_str, tzinfos=None)


def from_orm(model: Type['Base'], data: list):
    """Преобразование данных из БД в dataclass"""
    result = [model(**dict(i)) for i in data]

    return result
