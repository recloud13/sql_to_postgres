import datetime
import uuid
from dataclasses import dataclass, field
from typing import Optional

from models.base import TimeStamped, Base, CreatedStamped
from utils.utils import parse_datetime


@dataclass
class FilmWork(Base, TimeStamped):
    title: str = field(default='Unknown film')
    description: str = field(default=None)
    rating: float = field(default=0.0)
    creation_date: Optional[datetime.datetime] = field(default=None)
    file_path: Optional[str] = field(default=None)
    type: str = field(default=None)

    def __post_init__(self):
        super(FilmWork, self).__post_init__()
        if self.creation_date:
            self.creation_date = parse_datetime(self.creation_date)
        else:
            self.creation_date = None

        if not self.rating:
            self.rating = 0.0


@dataclass
class Genre(Base, TimeStamped):
    name: str = field(default='Unknown genre')
    description: str = field(default=None)


@dataclass
class Person(Base, TimeStamped):
    full_name: str = field(default='Unknown person')


@dataclass
class GenreFilmWork(Base, CreatedStamped):
    genre_id: uuid.UUID = field(default_factory=uuid.uuid4)
    film_work_id: uuid.UUID = field(default_factory=uuid.uuid4)


@dataclass
class PersonFilmWork(Base, CreatedStamped):
    film_work_id: uuid.UUID = field(default_factory=uuid.uuid4)
    person_id: uuid.UUID = field(default_factory=uuid.uuid4)
    role: str = field(default_factory=str)
