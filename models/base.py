import dataclasses
import datetime
import uuid
from dataclasses import dataclass, field
from typing import Optional, Type

from utils.utils import parse_datetime


@dataclass
class Base:
    id: uuid.UUID = field(default_factory=uuid.uuid4)

    def __iter__(self):
        return iter(dataclasses.astuple(self))


@dataclass
class CreatedStamped:
    created_at: datetime.datetime

    def __post_init__(self):
        self.created_at = parse_datetime(self.created_at)


@dataclass
class TimeStamped(CreatedStamped):
    updated_at: Optional[datetime.datetime]

    def __post_init__(self):
        super(TimeStamped, self).__post_init__()
        self.updated_at = parse_datetime(self.updated_at)


@dataclasses.dataclass
class LoadData:
    table_name: str
    model: Type[Base]
    get_query: str
