import contextlib
import sqlite3
from typing import Iterable

import psycopg2
from psycopg2.extensions import connection as _connection

from core import config
from databases.postgre import PostgresMigration
from databases.sqlite import SQLiteMigration
from models.domain import Person, Genre, FilmWork, GenreFilmWork, PersonFilmWork
from models.base import LoadData
from utils.utils import sqlite_connection


def load_from_sqlite(connection: sqlite3.Connection, pg_conn: _connection, load_queries: Iterable[LoadData]):
    """Основной метод загрузки данных из SQLite в Postgres"""
    postgres_saver = PostgresMigration(pg_conn)
    sqlite_loader = SQLiteMigration(connection)

    for load_data in load_queries:
        insert_data = sqlite_loader.load_all_data(load_data)
        postgres_saver.save_all_data(load_data, insert_data)


if __name__ == '__main__':
    load_queries = [
        LoadData('person', Person, 'SELECT * from person'),
        LoadData('genre', Genre, 'SELECT * from genre'),
        LoadData('film_work', FilmWork, 'SELECT * from film_work'),
        LoadData('genre_film_work', GenreFilmWork, 'SELECT * FROM genre_film_work'),
        LoadData('person_film_work', PersonFilmWork, 'SELECT * FROM person_film_work'),
    ]

    with sqlite_connection(config.DB_SQLITE_PATH) as sqlite_conn, contextlib.closing(
            psycopg2.connect(**config.POSTGRES_CONNECTION_CONFIG)
    ) as pg_conn:
        load_from_sqlite(sqlite_conn, pg_conn, load_queries)
