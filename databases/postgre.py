import dataclasses
from typing import Tuple, List, Any, Type

from psycopg2.extras import execute_batch, DictCursor

from databases.base import SaveDatabase, LoadDatabase
from models.base import Base, LoadData
from utils.utils import from_orm, join_list_to_string

PAGE_SIZE = 100

DB_SCHEMA = 'content'

PREPARE_NAME = 'insert_batch'


class PostgresMigration(LoadDatabase, SaveDatabase):
    def save_all_data(self, load_data: LoadData, data: List[Base]):
        if len(data) == 0:
            return

        with self.connection.cursor() as cur:
            self.__clear_table(cur, table_name=load_data.table_name)

            data_cls = data[0].__class__
            self.__set_postgres_prepare(cur, data_cls, load_data.table_name)

            insert_query = self.__generate_insert_query(data_cls)
            insert_data = self.__prepare_data_to_insert(data)
            execute_batch(cur, insert_query, insert_data, page_size=PAGE_SIZE)

            self.connection.commit()

    def load_all_data(self, load_data: LoadData) -> List[Base]:
        with self.connection.cursor() as cur:
            class_fields = join_list_to_string([i.name for i in dataclasses.fields(load_data.model)])
            query = f'SELECT {class_fields} FROM {load_data.table_name}'

            cur.execute(query)
            result = cur.fetchall()
            result = from_orm(load_data.model, result)

            return result

    def __clear_table(self, cur: DictCursor, table_name: str) -> None:
        """Полная очистка данных в заданной таблице"""
        query = f'TRUNCATE TABLE {DB_SCHEMA}.{table_name} RESTART IDENTITY CASCADE'
        cur.execute(query)

    def __generate_insert_query(self, cls: dataclasses.dataclass) -> str:
        """Формирование запросов на запуск Prepare исходя из данных dataclass'а"""
        class_fields = [i.name for i in dataclasses.fields(cls)]
        sql_vars = join_list_to_string(['%s' for _ in class_fields])

        insert_query = f'EXECUTE {PREPARE_NAME} ({sql_vars})'

        return insert_query

    def __set_postgres_prepare(self, cur: DictCursor, cls: Type[Base], table_name: str):
        """Создание Prepare для сессии postgres"""
        class_fields = [i.name for i in dataclasses.fields(cls)]

        sql_vars = join_list_to_string([f'${i}' for i in range(1, len(class_fields) + 1)])
        class_fields = join_list_to_string(class_fields)

        clear_all_prepares = 'DEALLOCATE PREPARE ALL'
        prepare = f'PREPARE {PREPARE_NAME} AS INSERT INTO {DB_SCHEMA}.{table_name}' \
                  f'({class_fields}) VALUES ({sql_vars}) ON CONFLICT DO NOTHING'

        cur.execute(clear_all_prepares)
        cur.execute(prepare)

    def __prepare_data_to_insert(self, data: List[Base]) -> List[Tuple[Any]]:
        """Упаковка значений dataclass в массив кортежей для сохранения этих значений в БД"""
        return [tuple(item) for item in data]

