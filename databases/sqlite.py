import sqlite3
from typing import List

from databases.base import LoadDatabase
from models.base import Base, LoadData
from utils.utils import from_orm


class SQLiteMigration(LoadDatabase):
    def __init__(self, connection):
        connection.row_factory = sqlite3.Row
        super(SQLiteMigration, self).__init__(connection)

    def load_all_data(self, load_data: LoadData) -> List[Base]:
        curs = self.connection.cursor()
        curs.execute(load_data.get_query)

        data = curs.fetchall()

        result = from_orm(load_data.model, data)

        return result
