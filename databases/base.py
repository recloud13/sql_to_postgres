from abc import abstractmethod
from typing import List, TypeVar

from models.base import Base, LoadData

LoadedValue = TypeVar('LoadedValue', bound=Base)


class DatabaseMigration:
    def __init__(self, connection):
        self.connection = connection


class LoadDatabase(DatabaseMigration):
    @abstractmethod
    def load_all_data(self, load_data: LoadData) -> List[LoadedValue]:
        """Получение данных из БД"""
        raise NotImplementedError


class SaveDatabase(DatabaseMigration):
    @abstractmethod
    def save_all_data(self, load_data: LoadData, data: List[LoadedValue]) -> None:
        """Сохранение полученных данных в БД"""
        raise NotImplementedError
